﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownController : MonoBehaviour {

    //Public variables
    public float speed;


    void FixedUpdate()
    {

        //Player basic control
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += new Vector3(0, speed, 0);
            //movementDirection = MOVEMENTDIRECTION.UP;
        }
        else
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += new Vector3(0, -speed, 0);
            //movementDirection = MOVEMENTDIRECTION.DOWN;
        }
        else
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += new Vector3(-speed, 0, 0);
            //movementDirection = MOVEMENTDIRECTION.LEFT;
        }
        else
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += new Vector3(speed, 0, 0);
            //movementDirection = MOVEMENTDIRECTION.RIGHT;
        }
        else { } 
            //movementDirection = MOVEMENTDIRECTION.NONE;

        /*if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(speed * Time.deltaTime, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(speed * Time.deltaTime * -1, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.W))
        {
            rb.velocity = new Vector2(rb.velocity.x, speed * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.velocity = new Vector2(rb.velocity.x, speed * Time.deltaTime * -1);
        }
        else
        {
            rb.velocity = new Vector2(0f, 0f);
        }*/

    }


}
