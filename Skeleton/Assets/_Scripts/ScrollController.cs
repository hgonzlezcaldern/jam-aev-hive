﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollController : MonoBehaviour {

    //Private variables
    private Rigidbody2D rb;
    private bool isJumping;
    private bool isDoubleJumping;

    //Public variables
    public float speed;
    public float jumpForce;
    public bool canDoubleJump;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        isJumping = false;
        isDoubleJumping = false;
    }


    private void Update()
    {
        if (Input.GetButtonDown("Jump") && (!isJumping || !isDoubleJumping))
        {
            Jump();
        }
    }


    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(speed * Time.deltaTime, rb.velocity.y);
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(speed * Time.deltaTime * -1, rb.velocity.y);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        
    }


    //Control de colisiones para el suelo
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Platform")
        {
            isJumping = false;
            isDoubleJumping = false;
        }
    }


    //Salto y doble salto (si permitido, mediante parámetro canDoubleJump) del personaje
    private void Jump()
    {
        Debug.Log("Jumping");

        if (isJumping && !isDoubleJumping && canDoubleJump)
        {
            isDoubleJumping = true;
        }

        rb.velocity = new Vector3(rb.velocity.x, 0f);
        rb.AddForce(new Vector2(0f, jumpForce));

        isJumping = true;
    }
}
