﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {	

    public void LoadSideScrollLevel()
    {
        SceneManager.LoadScene("SideScrollLevel");
    }

    public void LoadTopDownLevel()
    {
        SceneManager.LoadScene("TopDownLevel");
    }

    public void LoadOptions()
    {
        SceneManager.LoadScene("Options");
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main_menu");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
